<?php

/**
 * @file
 * Provides the interfaces for node based access control
 * 
 * This module was developed by Madcap, open.madcap.nl
 *
 * @author Raymond Muilwijk <r.muilwijk@madcap.nl>
 */

/**
 * Implementation of hook_menu().
 * @see hook_menu()
 *
 * @param boolean $may_cache
 * @return array
 */
function node_access_control_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/node_access_control',
      'title' => t('Node Access Control'),
      'type' => MENU_NORMAL_ITEM,
      'description' => t('Add permissions for use with node access control.'),
      'callback' => 'node_access_control_settings',
      'access' => user_access('administer site configuration'),
    );
    $items[] = array(
      'path' => 'admin/settings/node_access_control/delete',
      'type' => MENU_CALLBACK,
      'callback' => 'node_access_control_delete',
      'access' => user_access('administer site configuration'),
    );
  }

  return $items;
}

/**
 * Implementation of hook_form_alter().
 * @see hook_form_alter()
 *
 * @param string $form_id
 * @param array $form
 */
function node_access_control_form_alter($form_id, &$form) {
  if (user_access('administer node access')) {
    if ($form_id == 'node_type_form') {
      $form['workflow']['node_access_control_enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('Control Access'),
        '#description' => t('Allow permissions for view, updating and deleting of nodes of this type to be dynamically generated'),
        '#default_value' => variable_get('node_access_control_enabled_' . $form['#node_type']->type, FALSE)
      );
    }

    if ($form['#id'] == 'node-form' && node_access_control_valid_type($form['#node']->type)) {
      $form['node_access_control'] = array(
        '#type' => 'fieldset',
        '#title' => t('Node Access Control'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#tree' => TRUE,
        '#weight' => 8
      );

      $options = array_merge(array(t('No access control')), node_access_control_get_permissions());

      $form['node_access_control']['perm_view'] = array(
        '#type' => 'select',
        '#title' => t('View permission'),
        '#options' => $options,
        '#default_value' => node_access_control_get_perm($form['#node'], 'view'),
      );
      $form['node_access_control']['perm_update'] = array(
        '#type' => 'select',
        '#title' => t('Update permission'),
        '#options' => $options,
        '#default_value' => node_access_control_get_perm($form['#node'], 'update'),
      );
      $form['node_access_control']['perm_delete'] = array(
        '#type' => 'select',
        '#title' => t('Delete permission'),
        '#options' => $options,
        '#default_value' => node_access_control_get_perm($form['#node'], 'delete'),
      );
    }
  }

  // This part is not for the interface, it adds a submit function to permissions that are being saved
  if ($form_id == 'user_admin_perm') {
    $form['#submit'] = array_merge(
      array('node_access_control_permission_set_role_cache_submit' => array()),
      $form['#submit'],
      array('node_access_control_permission_rebuild_access_submit' => array())
    );
  }
}

/**
 * Callback for the 'admin/settings/node_access_control' path
 *
 * @return string
 */
function node_access_control_settings() {
  $output = '';
  $perms = node_access_control_get_permissions();
  $header = array(t('Permission'), t('Operations'));

  $output .= drupal_get_form('node_access_control_add_permission_form');

  $rows = array();
  foreach ($perms as $perm) {
    $rows[] = array($perm, l(t('Delete'), 'admin/settings/node_access_control/delete/' . $perm));
  }

  $output .= theme('table', $header, $rows);

  return $output;
}

/**
 * Form for adding permissions to node access control
 *
 * @return array
 */
function node_access_control_add_permission_form() {
  $form = array();

  $form['node_access_control'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add a new permission'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => FALSE,
  );
  $form['node_access_control']['permission'] = array(
    '#type' => 'textfield',
    '#title' => t('Permission name'),
    '#description' => t('The name of the permission you want to add'),
    '#required' => TRUE
  );

  $form['node_access_control']['add'] = array(
    '#value' => t('Add'),
    '#type' => 'submit'
  );

  return $form;
}

/**
 * Validate function for node_access_control_add_permission_form
 *
 * @param string $form_id
 * @param array $form
 */
function node_access_control_add_permission_form_validate($form_id, $form) {
  $all_perms = module_invoke_all('perm');

  if (in_array($form['permission'], $all_perms)) {
    form_set_error('permission', t('This permission already exists'));
  }
  if (check_plain($form['permission']) != $form['permission']) {
    form_set_error('permission', t('You are not allowed to use special characters'));
  }
  if (trim($form['permission']) != $form['permission']) {
    form_set_error('permission', t('You are not allowed to begin or end with spaces'));
  }
}

/**
 * Submit function for node_access_control_add_permission_form
 *
 * @param string $form_id
 * @param array $form
 */
function node_access_control_add_permission_form_submit($form_id, $form) {
  $perms = node_access_control_get_permissions();

  $perms[$form['permission']] = $form['permission'];

  node_access_control_set_permissions($perms);
}

/**
 * Callback for 'admin/settings/node_access_control/delete'
 *
 * @return string
 */
function node_access_control_delete() {
  $perm = arg(4); // Path: 'admin/settings/node_access_control/delete/$permission'

  $perms = node_access_control_get_permissions();

  if (!isset($perms[$perm])) {
    return MENU_ACCESS_DENIED;
  }

  drupal_set_title(t('Node Access Control Delete: !perm', array('!perm' => $perm)));

  $rs = db_query(
    "SELECT DISTINCT nid
    FROM {node_access_control}
    WHERE (perm_view='%s' OR perm_update='%s' OR perm_delete='%s')",
    $perm, $perm, $perm
  );
  
  if (db_num_rows($rs) > 0) {
    $nodes = array();
    while ($row = db_fetch_array($rs)) {
      $node = node_load($row['nid']);
      $nodes[] = l($node->title, 'node/' . $node->nid);
    }

    drupal_set_message(t('Some nodes are still using this permission, please remove them before deleting this permission.'), 'error');
    return theme('item_list', $nodes);
  }

  return drupal_get_form('node_access_control_delete_form', $perm);
}

/**
 * Form for deleting permissions generated by node access control
 *
 * @param string $perm
 * @return array
 */
function node_access_control_delete_form($perm) {
  $form = array();

  $form['permission'] = array(
    '#type' => 'value',
    '#value' => $perm
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete "!perm"?', array('!perm' => $perm)),
    'admin/settings/node_access_control',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit function for node_access_control_delete_form
 *
 * @param string $form_id
 * @param array $form
 * @return string
 */
function node_access_control_delete_form_submit($form_id, $form) {
  $perms = node_access_control_get_permissions();

  unset($perms[$form['permission']]);
  node_access_control_set_permissions($perms);

  return 'admin/settings/node_access_control';
}
