<?php

/**
 * @file
 * Provides helper functions for node access control
 * 
 * This module was developed by Madcap, open.madcap.nl
 *
 * @author Raymond Muilwijk <r.muilwijk@madcap.nl>
 */

/**
 * Helper function for loading a node's access control
 *
 * @param object $node
 */
function node_access_control_load_node(&$node) {
  if (!isset($node->node_access_control)) {
    $rs = db_query("SELECT * FROM {node_access_control} WHERE nid=%d AND vid=%d", $node->nid, $node->vid);
    $node->node_access_control = db_fetch_array($rs);
  }
}

/**
 * Helper function for getting all roles and their permissions
 *
 * @return array
 */
function node_access_control_get_all_roles($reset = FALSE) {
  static $roles = NULL;

  if ($roles == NULL || $reset == TRUE) {
    $rs = db_query("SELECT r.rid, p.perm FROM {role} r JOIN {permission} p USING (rid)");
    while ($row = db_fetch_array($rs)) {
      $roles[$row['rid']] = $row['perm'];
    }
  }

  return $roles;
}

/**
 * Helper function for checking or a string of permissions has a operators permission
 *
 * @param string $total_permissions
 * @param string $op
 * @param object $node
 * @return boolean
 */
function node_access_control_has_perm($total_permissions, $op, &$node) {
  node_access_control_load_node($node);

  $perm = node_access_control_get_perm($node, $op);

  if ($perm == '0') {
    // Drupal sets view access default to true. When we don't have a permission for one of the op's "view" should return true.
    // "Update" and "delete" should return false.
    if ($op == 'view') {
      return TRUE;
    }

    return FALSE;
  }

  return (strpos($total_permissions . ', ', "$perm, ") !== FALSE);
}

/**
 * Helper function for retrieving all the node access controlled node types
 *
 * @return array
 */
function node_access_control_get_types() {
  $types = node_get_types();

  $nac_types = array();

  foreach ($types as $type => $type_object) {
    if (variable_get('node_access_control_enabled_' . $type, FALSE) == TRUE) {
      $nac_types[] = $type;
    }
  }

  return $nac_types;
}

/**
 * Helper function for checking whether a node type is controlled by node access or not
 *
 * @param string $type
 * @return boolean
 */
function node_access_control_valid_type($type) {
  return variable_get('node_access_control_enabled_' . $type, FALSE);
}

/**
 * Helper function for retrieving the permissions defined by this module
 *
 * @return array
 */
function node_access_control_get_permissions() {
  return variable_get('node_access_control_permissions', array());
}

/**
 * Helper function for settings the permissions defined by this module
 */
function node_access_control_set_permissions($perms) {
  ksort($perms);
  variable_set('node_access_control_permissions', $perms);
}

/**
 * Helper function for returning a node's permission for a operator
 *
 * @param object $node
 * @param string $op
 * @return string
 */
function node_access_control_get_perm(&$node, $op) {
  node_access_control_load_node($node);

  // We want this to be E_ALL safe code so we have to check or it's all set
  if (isset($node->node_access_control) && is_array($node->node_access_control) && isset($node->node_access_control['perm_' . $op])) {
    return $node->node_access_control['perm_' . $op]; // Return actual value
  }

  return '0';
}

/**
 * Helper function for checking whether or not access control is set.
 *
 * @param object $node
 * @return boolean
 */
function node_access_control_isset(&$node) {
  node_access_control_load_node($node);
  
  if (!isset($node->node_access_control) || !is_array($node->node_access_control)) {
    return false;
  }
  
  $nac_view = node_access_control_get_perm($node, 'view');
  $nac_update = node_access_control_get_perm($node, 'update');
  $nac_delete = node_access_control_get_perm($node, 'delete');

  return ($nac_view != '0' || $nac_update != '0' || $nac_delete != '0');
}
